from django.contrib import admin

from .models import CsvUpload, Customer, InvoiceItem, ClientMrr

admin.site.register(CsvUpload)
admin.site.register(Customer)
admin.site.register(InvoiceItem)
admin.site.register(ClientMrr)
