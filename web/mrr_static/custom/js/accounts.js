$(document).ready( function() {

	$(".content.body").on("click", ".panel-heading.cursor-pointer:not(.opened)", function() {
		var account_id = $(this).attr("data-id");

		loadMRRData(account_id);
	});

	$(".content.body").on("click", ".panel-heading.cursor-pointer.opened", function() {
		var account_id = $(this).attr("data-id");

		$(this).removeClass("opened");
		$(".panel-heading[data-id='" + account_id + "']").closest(".panel").find(".panel-body").addClass("hidden");
		$(".panel-heading[data-id='" + account_id + "']").closest(".panel").find("tbody tr:not(.tr-clone)").remove();
	});
});


function loadMRRData(account_id) {

	var api_call = '/analytics/?format=json&client=' + account_id;

	$.ajax({
        type : "GET",
        url : api_call,
        dataType : "json",
        contentType : "application/json",
        success : function(data) {

        	var body = $(".panel-heading[data-id='" + account_id + "']").addClass("opened").closest(".panel").find("tbody");

        	$.each(data, function(i, object) {
        		var clone = body.find(".tr-clone").clone().removeClass("tr-clone hidden");

        		clone.find("[data-type='bookings']").text(rounder(object.bookings));
	        	clone.find("[data-type='churn_mrr']").text(rounder(object.churn_mrr));
	        	clone.find("[data-type='contraction_mrr']").text(rounder(object.contraction_mrr));
	        	clone.find("[data-type='expansion_mrr']").text(rounder(object.expansion_mrr));
	        	clone.find("[data-type='month']").text(object.month);
	        	clone.find("[data-type='new_mrr']").text(rounder(object.new_mrr));
	        	clone.find("[data-type='total']").text(rounder(object.total));

	        	body.append(clone);
	        });

        	$(".panel-heading[data-id='" + account_id + "']").closest(".panel").find(".panel-body").removeClass("hidden");
        }
    });
}